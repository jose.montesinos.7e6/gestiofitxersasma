import java.io.File
import java.time.LocalDate
import kotlin.io.path.*
import kotlin.reflect.typeOf

/**
 * Gestió d'usuaris del gimnàs del grup ASMA: es tracta d'una petita 'aplicació' que pot importar usuaris de l'antic arxiu .txt al nou arxiu .txt, modificar els usuaris actius i fer
 * backups també en format .txt.
 * Els usuaris estàn guardats en un/s antic/s arxiu/s .txt anomenat/s OldFileUsers'$i'.txt a la carpeta Import i les seves dades estàn desorganitzades.
 * Les dades de cada usuari són: Id, Nom, Número de telèfon, Email, Actiu:True/False, Bloquejat: True/False.
 * L'aplicació, al iniciars-se,  agafarà els usuaris de l'àntic arxiu .txt, modifificarà l'estructura de dades, implementarà un id ascendent per a cada usuari i nomès traspassarà al nou arxiu .txt els
 * que tinguin els camps 'Actiu= True' i 'Bloquejat= False'. Aquest nou arxiu es guardarà com userData.txt a la carpeta Data.
 * L'estructura de dades ara cambiarà i en el nou arxiu cada valor de cada usuari es separarà per ';'.
 * Després, es mostra un menú principal. des d'on podrèm seleccionar quina acció volém fer.
 * Les opcions són:
 * 1. Traspassar dades: Fa l'acció de traspassar els usuaris de l'antic arxiu al nou arxiu tal com he especificat abans.
 * 2. Crear Usuari: Et demana cada un dels camps del nou usuari excepte l'id, els introdueixes i et crea un nou usuari amb un id autoincrementat.
 * 3. Modificar Usuari: Permèt modificar els camps del nom, telèfon i mail de l'usuari seleccionat.
 * 4. Bloquejar/Desbloquejar usuari: Permèt cambiar el camp 'Bloquejat' de true a false o a la inversa de l'usuari seleccionat.
 * 5. Crear backup: Permèt creat un backup a la carpeta backups amb el nom 'AAAA-MM-DDuserData.txt', on AAAA-MM-DD es la data actual, de l'últim arxiu userData que tinguém.
 * 6. Sortir de l'aplicació: Et permèt sortir de l'aplicació, abans de sortir et crea un backup en cas de que l'usuari no n'hi hagi creat cap aquell mateix día.
 *
 * @author: Jose Montesinos
 * @version: 1.0
 */

/*Funció que verifica s'hi existeix un antic arxiu OldFileUsers.txt
 *Si existeix, agafa cada línea d'informació i la transforma a un element d'una llista,
 *després agafa cada un d'aquests elements i els separa per el ';', creant una llista d'usuaris.
 *A continuació, envía aquest usuari a la funció 'crearNuevoSistema' i elimina l'arxiu antic OldFileUsers.
 *Si no existeix l'antic arxiu OldFileUsers, simplement et printa que no existeix.
 *@parem: res.
 *@return: res
 */
fun verificar(){
    var existOldFile = true   //variable que indica si existeix l'antic arxiu .txt
    var i = 1  //contador que indica el número de l'arxiu OldFileUsers'$i'
    while(existOldFile){
        val OldFile = Path("./Import/OldFileUsers${i}.txt")
        existOldFile = OldFile.exists()  //Canviarà el valor si no troba l'arxiu OldFileUsers'$i'.txt
        if(existOldFile){
            val lineas = OldFile.readLines().toMutableList()  //cada línea escrita a l'arxiu OldFileUsers es un element d'una mutableList
            //println(lineas)
            for(j in 0..lineas.size-1) {
                val usuarios = lineas[j].split(';')    //Separém  cada linea en diferents usuaris separats per ';'
                for(k in 0..usuarios.size-1){
                    println(usuarios[k])
                    crearNuevoSistema(usuarios[k])   //Cridem a la funció que gestionará la creació d'un nou arxiu userData
                }
            }
            OldFile.deleteIfExists()  //Eliminen l'antic arxiu
        }else{
            if(i-1==0) println("No existeix cap arxiu OldFileUsers per extreure els usuaris")
            else{println("Existeix fins l'arxiu OldFileUsers${i-1} a la base de dades antiga d'on extreure els usuaris")}
        }
        i++
    }
}

/*Funció que agafa cada un dels usuaris creats a la funció 'verificar', separa cada uns dels camps de l'usuari delimitats per ',' i els fica a una llista.
 *Si els camps 'Actiu!=false' o 'Bloquejat!=true', agafa la llista dels valors de l'usuari i la envía a la funció 'idAscendente', selecciona el nou arxiu userData.txt a la carpeta Data i escriu el nou usuari en aquest arxiu.
 +@parem: String, block d'un usuari amb els valors de l'usuari separats per ',', tot en un string.
 *@return: res
 */
fun crearNuevoSistema(usuarios:String){
    val listaUsuarios= usuarios.split(',').toMutableList()   //per a cada usuari, separém els valors per ','
    if(listaUsuarios[4]!="false"  || listaUsuarios[5]!="true"){     //si el camp actiu!=false o bloquejat!=true
        idAscendente(listaUsuarios)   //Cridem a la funció que ens indicará l'id de l'usuari

        val nuevoPath = Path("./Data/userData.txt")  //arxiu que s'ha creat amb la funció 'idAscendente'

        //afegim l'usuari a l'arxiu userData
        nuevoPath.appendText(listaUsuarios[0] + ";" +listaUsuarios[1] + ";"+listaUsuarios[2] + ";"+listaUsuarios[3] + ";"+listaUsuarios[4] + ";"+listaUsuarios[5] + ";" +"\n")

    }

}

/*Funció que agafa la llista de valors d'un usuari, comprova si existeix un arxiu 'userData', en cas de que no existeixi el crea i agafa l'usuari enviat i li possa el camp id=1,
 *si ja existeix l'arxiu 'userData', compta els usuaris que hi han, registra el número en un comptador i li posa el comptador com a id del nou usuari.
 *@parem: MutableList<String>, llista amb els valors de l'usuari enviat per la funció 'crearNuevoSistema'.
 *@return: res.
 */
fun idAscendente(listaUsuarios:MutableList<String>){
    val nuevoPath = Path("./Data/userData.txt")
    if(!nuevoPath.exists()){  //Si no existeix l'arxiu userData
        nuevoPath.createFile()   //El creém
        listaUsuarios[0]="1"   //I l'usuari passat com a paràmetre tindrá l'id = 1
    }else{   //Pero si existeix l'arxiu userData
        var i = 1  //Iniciarèm un contador dels usuaris a l'arxiu userData
        nuevoPath.forEachLine{
            i++     //Comptarà els usuaris que n'hi han amb un bucle
        }
        listaUsuarios[0]="$i"   //L'id de l'usuari passat com a paràmetre, tindrà l'id del comptador 'i'
    }
}

/*Funció que et demana el nom, el cognom, el tèlefon i l'email amb certes restriccions. Agafa cadascún d'aquests valors i els fica a una llista.
 *Si ja existeix un arxiu userData, compta els usuaris que n'hi han, enregistra el número en un contador i li posa aquest contador al id del nou usuari,
 *juntament amb els valors de la llista. Finalment, escriu les dades a l'arxiu userData i l'informa a l'usuari per pantalla.
 *Si no existeix un arxiuUserData, crea un, li possa '1' com a id de l'usuari i l'introdueix a la llista juntament amb els valors que s'habian introduit.
 *Finalment, escriu les dades de l'usuari a l'arxiu 'userData' i l'informa a l'usuari per pantalla.
 * @parem: res.
 * @return: res.
 */
fun crearUsuario(){
    val nuevoPath = Path("./Data/userData.txt")
    val existNuevoPath = nuevoPath.exists()  //boolea true/false que ens indica si existeix l'arxiu userData

    println("Introdueix el nom i el cognom:")
    val nombreUsuario = readln()   //nom i cognom de l'usuari que volém crear
    println("Ara introdueix un número de telèfon")
    var telefono = readln()       //telèfon de l'usuari que volèm crear
    while(telefono.length!=9){
        println("Introdueix un número de telèfon de 9 carácteres")
        telefono=readln()
    }
    println("Introdueix un correu electrònic: ")
    var correo = readln()        //correu del nou usuari
    while(!correo.contains("@")){
        println("Introdueïx el caràcter @ per especificar que és un correu electrònic")
        correo = readln()
    }

    val listaUsuario = mutableListOf<String>()    //Llista on ficarèm els valors del nou usuari

    if(existNuevoPath){
        var i = 1   //Iniciarèm un contador dels usuaris a l'arxiu userData
        nuevoPath.forEachLine{
            i++        //Comptarà els usuaris que n'hi han amb un bucle
        }
        //Afegim els valors introduits anteriorment per teclat a la llista de valors del nou usuari i l'id serà el contador després de passar per el bucle
        listaUsuario.add("$i")
        listaUsuario.add(nombreUsuario)
        listaUsuario.add(telefono)
        listaUsuario.add(correo)
        listaUsuario.add("true")
        listaUsuario.add("false")

        //val listaUsuarioStr = listaUsuario.toString()
        nuevoPath.appendText(listaUsuario[0] + ";" +listaUsuario[1] + ";"+listaUsuario[2] + ";"+listaUsuario[3] + ";"+listaUsuario[4] + ";"+listaUsuario[5] +";"+"\n")   //Afegim el nou usuari
        println("Ha sigut registrat com:")
    }else{      //Si no existeix l'arxiu userData
        nuevoPath.createFile()   //El creém
        listaUsuario.add("1")    //L'id del nou usuari serà el primer del nou arxiu userData
        listaUsuario.add(nombreUsuario)
        listaUsuario.add(telefono)
        listaUsuario.add(correo)
        listaUsuario.add("true")
        listaUsuario.add("false")

        //val listaUsuarioStr = listaUsuario.toString()
        nuevoPath.appendText(listaUsuario[0] + ";" +listaUsuario[1] + ";"+listaUsuario[2] + ";"+listaUsuario[3] + ";"+listaUsuario[4] + ";"+listaUsuario[5] +";"+"\n")  //Afegim el nou usuari
        println("S'ha creat un nou arxiu a la carpeta Data.\n" +
                "Reinicia l'aplicació perque es mostri l'arxiu a la barra del projecte on s'ha registrat el següent usuari:")
    }

    //Informem a l'usuari per pantalla que s'ha creat el nou usuari
    println(" *Id: ${listaUsuario[0]} \n" +
            " *Nom y cognom: ${listaUsuario[1]} \n" +
            " *Número de telèfon: ${listaUsuario[2]}\n" +
            " *Email:  ${listaUsuario[3]}\n")
}

/*Funció que comprova si existeix l'arxiu userData.
 *Si existeix, compta cada un dels usuaris que hi han a l'arxiu 'userData', per cada número del comptador, comprova si és igual a l'id introduit per l'usuari, si és aixi et demana el nom, cognom, telefon i email amb certes restriccions per a modificar-los.
 *Escriu aquestes noves dades a la posició on s'ha quedat el comptador i es trenca el bucle.
 * Si cap número del comptador és igual a l'id introduit per l'usuari, et notifica per pantalla que no existeix cap usuari amb aquella id.
 * Si no existeix l'arxiu 'userData', notifica a l'usuari que no existeix i que ha de crear un.
 * @parem:Int, id de l'usuari a modificar.
 * @return: res.
 */
fun modifyUser(id: Int) {
    val path = Path("./Data/userData.txt")
    if (path.exists()) {     //Si existeix l'arxiu userData
        var campos: List<String>    //Llista on ficarém els valors de l'usuari a modificar
        val lines = path.readLines().toMutableList()   //Llista mutable on fiquém cada línea de l'arxiu userData
        var userExists = false   //variable que ens indica si l'usuari introduit per teclat existeix, el valor inicial serà false
        for (i in 0 until lines.size) {    //Bucle que analitza cada element de la llista on em ficat les línies d'usuaris de l'arxi userData
            campos = lines[i].split(';')  //Variable que guarda l' usuari, separant per ';' cada línea escrita de l'arxiu userData
            if (campos[0].toInt() == id) {   //Si ha trobat un usuari amb id igual a l'introduit
                userExists = true     //Existeix l'usuari
                println("Introdueix el nom i el cognom de l'usuari:")
                val nom = readLine()  //Introduim el nou nom i cognom
                println("Introdueix el número de telèfon: ")
                var telf = readln()   //Introduim el nou telèfon
                while(telf.length!=9){
                    println("Introdueix un número de telèfon de 9 caràcters")
                    telf=readln()
                }
                println("Introdueix un correu electrònic: ")
                var email = readln()   //Introduim el nou email
                while(!email.contains("@")){
                    println("Introdueïx el caràcter @ per especificar que és un correu electrònic")
                    email = readln()
                }
                lines[i] = "$id;$nom;$telf;$email;${campos[4]};${campos[5]};" //La nova linea a la posició i la modifiquem amb les noves dades i deixem els valors d'actiu i bloquejat que teníen abans
                println("El nou usuari amb id ${id} és: \n" +
                        " - Nom i Cognom: ${nom}\n" +
                        " - Telèfon: ${telf}\n" +
                        " - Email: ${email}\n" +
                        " - Actiu: ${campos[4]}" +
                        " - Bloquejat/Desbloquejat: ${campos[5]}\n")
                break  //Trenquém el bucle, no cal analitzar més usuaris
            }
        }
        if (userExists) {     //Un cop acabat l'anterior procès, si aquets usuari existeix, l'inserím al fitxer userData i el separem per '\n'
            path.writeText(lines.joinToString("\n"))

        } else {     //Si no existeix l'usuari informém per pantalla
            println("No existeix cap usuari amb aquest ID")
        }
    } else {    //Si no existeix l'arxiu userData
        println("L'arxiu UserData.txt no existeix. Primer has de crear un.")
    }
}

/*Funció que bloqueja o desbloqueja un usuari.
 *Primer comprova si existeix l'arxiu userData, si existeix, compta els usuaris que n'hi han i compara cada un dels números del contador amb l'id introduuit per l'usuari, si l'id es igual a un d'aquests números,
 *guarda el número del comptador, separa els valors d'aquests usuari en una llista anomenada 'campos', aquesta llista es renombra com a una llista mutable anomenada 'camposModificados' i depenent del valor 'bloquejat' de la llista 'campos',
 *el valor 'bloquejat' de la llista camposModificados, serà el contrari de la llista 'campos'.
 *A continuació agafa la línea de l'usuari amb l'id que hem guardat del comptador amb anterioritat i li assigna els valors de la llista 'camposModificados' juntant-lo tot amb un sol string.
 *Finalment, aquests string derivat de la llista 'camposModificados' es escrit a l'arxiu userData separat per a cada línea escrita a l'arxiu (cada línea és un usuari).
 *Si no hi ha cap número del comptador igual a l'id s'informa per pantalla que aquest usuari no existeix.
 *Si no existeix cap arxiu userData, se l'informa a l'usuari per pamtalla.
 *@parem: Int, id de l'usuari a bloquejar o desbloquejar.
 *@return: res.
 */
fun blockUnblock(id: Int){
    val path = Path("./Data/userData.txt")
    if (path.exists()) {   //Si existeix l'arxiu userData
        var userExists = false    //Variable que ens indica si existeix l'usuari introduit
        var campos: List<String>     //Llista on ficarém els valors de l'usuari a modificar
        var camposModificados: MutableList<String>     //Llista on 'transpasarém els valors de la llista 'campos''
        var saveIndex: Int   //Variable on guardarem l'id de l'usuari trobat a l'arxiu userData
        val lines = path.readLines().toMutableList()   //Llegim cada linea de l'arxiu userData y les convertim a valors d'una llista de les línies
        for (i in 0 until lines.size) {   //Bucle que llegeix cada línea
            campos = lines[i].split(';')    //Creem l' usuari separant cada linea per ';' i el fiquem a la variable 'campos'
            if (campos[0] == id.toString()) {   //campoc[0] el el valor de l'id, si aquest es igual a l'id introduit per l'usuari llavor existeix l'usuari
                userExists = true  //Existeix l'usuari
                saveIndex = i   //Aquest valor del contador del bucle for el guardem com a index de l'usuari trobat
                camposModificados = campos.toMutableList()    //Traspassem els valors de la lista 'campos' a 'camposModificados'
                if (campos[5] == "true") {     //Canviem els valors de bloquejat/desbloquejat de la llista 'camposModificados' segons el valor inicial a la llista 'campos'
                    camposModificados[5] = "false"
                } else {
                    camposModificados[5] = "true"
                }
                lines[saveIndex] = camposModificados.joinToString(";")  //introduim a la llista d'usuaris el nou usuari amb els camps modificats
                println("S'ha modificat el camp de Bloquejat a ${camposModificados[5]} de l'usuari amb id ${id}")
            }
        }
        if(userExists) {   //Si ha exisitit l'usuari i hem fet els canvis pertinents llavors:
            path.writeText(lines.joinToString(System.lineSeparator()))      //Escribim els usuaris de la llista amb les modificacions a l'arxiu userData separats per un salt de línea
        }else{   //Si no existeix l'usuari introduit per teclat, informem per pantalla.
            println("No existeis un usuari amb aquesta ID")
        }
    }else{    //Si no existeix l'arxiu userData, informém per pantalla
        println("L' arxiu userData.txt no existeix. Primer has de crear un.")
    }

}

/*Funció que crea backups amb la data escrita al nom del fitxer.
 *Primer de tot comprova si existeixen els arxius backup i userData, d'aquest principi hi pot haber 4 posibilitats.
 * 1. Que existeixi l'arxiu backup i l'arxiu 'userData'. Elimina l'anterior backup, llegeix els usuaris a l'arxiu userData, crea un nou backup i escriu els usuaris llegits al nou backup.
 * Finalment l'informa de k'operació a l'usuari per pantalla.
 * 2. Que existeixi l'arxiu backup pero que no existeixi l'arxiu userData. Simplement informa a l'usuari per pantalla que no s'ha creat un nou backup.
 * 3. Que no existeixi l'arxiu backup pero que si existeixi l'arxiu userData. Fa el mateix que a la primera posibilitat pero sense borrar l'anterior arxiu backup perque no n'hi ha cap.
 * 4. Que no existeixi l'arxiu backup i que tampoc existeixi l'arxiu userData. L'informa a l'usuari que no pot crear un backup perque no existeix l'arxiu userData.
 *@parem: res,
 *@return: res.
 */
fun crearBackUp(){
    val date = LocalDate.now()    //Variable on guardem la data actual
    //println(date)
    val backup = Path("./Backups/${date}userData.txt")   //Variable que contindrá el nom del nou backup
    val data = Path("./Data/userData.txt")
    if(backup.exists()){   //Si ja existeix el backup
        if(data.exists()){   //Si existeix l'arxiu userData
            backup.deleteIfExists()     //Borrem l'antic backup
            val inscritos = data.readText()    //Llegim l'arxiu userData
            //print(inscritos)
            backup.createFile()     //Creem nou backup
            backup.writeText(inscritos)   //Inserim les dades de userData al nou backup
            println("S'ha actualitzat un nou backup a la carpeta backups")  //Informem per pantalla
        }else{   //Si no existeix l'arxiu userData
            println("No s'ha creat cap nou backUp perque no hi ha cap arxiu userData a la carpeta Data.")    //Informem per pantala
        }
    }else{   //Si no exosteix un backup anterior
        if(data.exists()){   //Si existeix l'arxiu userData
            val inscritos = data.readText()    //Llegim els valors de l'arxiu userData
            //print(inscritos)
            backup.createFile()   //Creem backup
            backup.writeText(inscritos)    //inserim els valors llegits de userData al backup
            println("S'ha creat un nou backup a la carpeta backups")
        }else{   //Si no existeix l'arxiu userData
            println("No s'ha creat cap backUp perque no hi ha cap arxiu userData a la carpeta Data.")
        }
    }
    //
    //data.copyTo(backup, true)
}

/*Funció que mostra el menú de les diferents operacions que pot fer l'usuari a l'aplicació.
 *@parem: res.
 *@return: res.
 */
fun menu() {
    var salir = true   //Variable que ens permet sortir del menú principal
    do {
        println( """Selecciona l'acció que vols fer:
        | ##############################
        | 1. Traspassar dades
        | 2. Crear Usuari
        | 3. Modificar usuari
        | 4. Des/Bloquejar Usuari
        | 5. Crear backup
        | 0. Sortir de l'aplicació
        | ##############################
        """.trimMargin())
        when(readln()){
            "1"->{
                verificar()
            }
            "2"->{
                crearUsuario()
            }
            "3"->{
                println("Introdueix l'Id de l'usuari que vols modificar: ")
                val id = readln().toInt()
                modifyUser(id)
            }
            "4"->{
                println("Introdueix l'Id de l'usuari que vols bloquejar o desbloquejar: ")
                val id = readln().toInt()
                blockUnblock(id)

            }
            "5"->{
                crearBackUp()
            }
            "0"->{
                salir = false
                crearBackUp()
            }else->{
                println("Error, prém un botó del menú principal")
            }
        }
    }while (salir)
    println("Fins a reveure! Torna quant vulguis")

}

/*Funció principal on es crida l'inici del funcionament de l'aplicació.
 *@parem: res.
 *@return: res.
 */
fun main() {
    verificar()
    menu()
}