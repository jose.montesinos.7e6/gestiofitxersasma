# Aplicació del gimnàs ASMA mitjançant fitxers.txt

## Descripció

Gestió d'usuaris del gimnàs del grup ASMA: es tracta d'una petita 'aplicació' que pot importar usuaris de l'antic arxiu .txt al nou arxiu .txt, modificar els usuaris actius i fer
backups també en format .txt.
Els usuaris estàn guardats en un/s antic/s arxiu/s .txt anomenat/s OldFileUsers'$i'.txt a la carpeta Import i les seves dades estàn desorganitzades.
Les dades de cada usuari són: Id, Nom, Número de telèfon, Email, Actiu:True/False, Bloquejat: True/False.
L'aplicació, al iniciars-se,  agafarà els usuaris de l'àntic arxiu .txt, modifificarà l'estructura de dades, implementarà un id ascendent per a cada usuari i nomès traspassarà al nou arxiu .txt els
que tinguin els camps 'Actiu= True' i 'Bloquejat= False'. Aquest nou arxiu es guardarà com userData.txt a la carpeta Data.
L'estructura de dades ara cambiarà i en el nou arxiu cada valor de cada usuari es separarà per ';'.
Després, es mostra un menú principal. des d'on podrèm seleccionar quina acció volém fer.
Les opcions són:
 1. Traspassar dades: Fa l'acció de traspassar els usuaris de l'antic arxiu al nou arxiu tal com he especificat abans.
 2. Crear Usuari: Et demana cada un dels camps del nou usuari excepte l'id, els introdueixes i et crea un nou usuari amb un id   autoincrementat.
 3. Modificar Usuari: Permèt modificar els camps del nom, telèfon i mail de l'usuari seleccionat.
 4. Bloquejar/Desbloquejar usuari: Permèt cambiar el camp 'Bloquejat' de true a false o a la inversa de l'usuari seleccionat.
 5. Crear backup: Permèt creat un backup a la carpeta backups amb el nom 'AAAA-MM-DDuserData.txt', on AAAA-MM-DD es la data actual, de l'últim arxiu userData que tinguém.
 6. Sortir de l'aplicació: Et permèt sortir de l'aplicació, abans de sortir et crea un backup en cas de que l'usuari no n'hi hagi creat cap aquell mateix día.

## Estat del projecte

És una versió molt simple d'una aplicació que gestiona usuaris d'un gimnàs. Utilitza fitxers.txt per apendre a utilitzar-los a kotlin pero sería més adients utilitzar una base de dades relacional al núvol per emmagatzemar els usuaris i poder-los gestionar des de l'intelliJ a qualsevol lloc d'on disposem d'un ordinador. 

A més podríem introduir més dades de l'usuari, com el seu estat de salud, tipus d'activitats adients a ell/ella, tarifa a la que està apuntat/da, activitats que te disponible a partir de la seva tarifa, contractació d'un/una entrenador/a personal, etc pero per això caldría l'us de més taules a la base de dades i es tornaría més complexe. 

## Bugs trobats al programa

El primer bug i el més important que he trobat es que a l'arxiu 'OldFileUsers.txt' no es poden tenir salts de línea amb línies buides perque al trobar un espai en blanc el programa es trenca. El número indicat a l'esquerra de l'arxiu de text deu de ser igual al número de linies escrites i el cursor deixar-lo al final de l'última lletra del document.

Correcte:

1    12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;123FG34,Lara Gomez,689953247,lara1985@mail.com,false,true
2    12732461,Óscar García,697543215,oscar.garcia@mail.com,false,true;123FG34,Laura Rodriguez,689953247,laura1985@mail.com,true,false

Incorrecte:

1    12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;123FG34,Lara Gomez,689953247,lara1985@mail.com,false,true
2    12732461,Óscar García,697543215,oscar.garcia@mail.com,false,true;123FG34,Laura Rodriguez,689953247,laura1985@mail.com,true,false
3
4                     |(Cursor)

El segon bug que he trobat es que, les dades que s'escriuen a l'arxiu 'OldFileUsers.txt' ha de ser exactament igual al del exemple proposar a clase. Exemple: 12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;123FG34,Lara Gomez,689953247,lara1985@mail.com,false,true

Es pot veure que cada usuari es separat l'un de l'altre amb ';' pero l'últim no té al final el ';'. Si es posa, el programa es trenca perque al fer el .split(;) no troba cap element després i no el pot afegir a la llista d'usuaris. 

Correcte:

1    12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;123FG34,Lara Gomez,689953247,lara1985@mail.com,false,true

Incorrecte:

1    12732461,Àngel Sanz,697543215,angel.sanz@mail.com,true,false;123FG34,Lara Gomez,689953247,lara1985@mail.com,false,true; (';' al final de l'usuari)

El tercer bug es que quan no hi ha cap arxiu 'OldFileUsers.txt' i per tant no s'ha creat cap arxiu 'userData.txt', si vols introduir un nou usuari, aquests es crea en un nou arxiu 'userData.txt' pero no surt als arxius del projecte fins que no paras el programa, o bé amb el cuadrat vermell o bé amb l'opció sortir posada al menú de l'aplicació. 

Segur que hi han més bugs pero aquests són els més importants que poden donar problemes facilment si no es tenen en compte i que cal sapiguer. 

## Tecnologies utilitzades

Entorn de desenvolupament per a kotlin com pot ser IntellijIdea.
Creació d'arxius .txt dins del projecte creat. 

Si hi han dubtes de com està estructurat el readme, clica a 'edit' per veure el format dels bugs correctament. 
